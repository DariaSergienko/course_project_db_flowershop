<?php
core\Core::getInstance()->pageParams['title'] = 'Кошик';
/** @var array $cart */
session_start();
?>
<h2 class="mb-4 text-center">Кошик</h2>


<?php if (!empty($cart)) : ?>
    <form action="#" method="post" id="cart_form">
    <table class="table mb-3 align-middle">
        <thead>
        <tr>
            <th>№</th>
            <th>Назва товару</th>
            <th>Вартість одиниці</th>
            <th>Кількість</th>
            <th>Загальна вартість</th>
            <th></th>
        </tr>
        </thead>
        <?php $index = 1; foreach ($cart['products'] as $row) : ?>
            <tr>
                <td><?=$index ?></td>
                <td><?=$row['product']['name'] ?> </td>
                <td><?=$row['product']['price'] ?> грн.</td>

                <td>
                    <form method="post" action="/cart/change/<?=$row['product']['id']?>">
                        <div class="d-flex justify-content-between">
                            <input readonly style="height: 40px" required id="count" max="<?=$row['product']['count']?>" min="1" type="number" value="<?php echo $row['count']?>" class="form-control">
<!--                            <button style="height: 40px" class="btn btn-primary">Змінити</button>-->
                        </div>
                    </form>

                    <span id="count_error"></span>
                </td>

                <td><?=$row['product']['price'] * $row['count'] ?> грн.</td>
                <td><a href="cart/remove/<?=$row['product']['id']?>"><img class="" style="width: 24px; height:24px" src="/static/images/cross_alt.png"></a></td>
            </tr>
            <?php $index++; endforeach; ?>
        <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th>Загальна сума</th>
            <th><?=$cart['total_price']?> грн.</th>
            <th></th>
        </tr>
        </tfoot>
    </table>
    </form>
    <div>
        <a href="cart/clear" class="btn btn-danger">Очистити кошик</a>
        <a href="order/index" class="btn btn-success">Оформити замовлення</a>
    </div>

<?php else : ?>
    <div class="text-center">
        <div class="container mb-3">
            Кошик порожній.
        </div>
        <div class="mb-3">
            <a href="product/" class="btn btn-primary">Переглянути товари</a>
        </div>
    </div>
<?php endif; ?>

