
<?php
/** @var array|null $rows */
/** @var array|null $found_products */
core\Core::getInstance()->pageParams['title'] = 'Товари';
use models\User;

?>
<h2 class="h3 mb-4 fw-normal text-center">Список товарів</h2>
<?php if(User::isAdmin()) :?>
    <div class="d-flex justify-content-center">
        <a href="/product/add" class="btn btn-success mb-3">Додати товар</a>
    </div>
<?php endif; ?>
    <form method="post" action="/product/search">
        <div class="d-flex justify-content-between mb-4 mt-4">
            <input required style="height: 40px; width: 1100px" type="search" class="form-control" placeholder="Шукайте товари тут!" aria-label="Search" name="search_statement">
            <button style="height: 40px; width: 175px;" class="btn-primary btn">Знайти товари</button>
        </div>
    </form>

<form method="post" action="/product/filter">
    <div class="d-flex justify-content-between mb-4 mt-4">
        <p>Ціна:</p>
        <input required style="height: 40px; width: 100px" type="text" class="form-control" placeholder="0" aria-label="Min_price" name="min_price">-
        <input required style="height: 40px; width: 100px" type="text" class="form-control" placeholder="10000" aria-label="Max_price" name="max_price">
        <button style="height: 40px; width: 175px;" class="btn-primary btn">Фільтрувати</button>
    </div>
</form>


<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php if (!empty($found_products)) : ?>
        <?php foreach ($found_products as $row) : ?>
            <div class="col">
                <a href="/product/view/<?= $row['id'] ?>" class="card-link">
                    <div class="card">
                        <?php $filePath = 'files/product/'.$row['photo']; ?>
                        <?php if(is_file($filePath)) : ?>
                            <img src="/<?=$filePath ?>" class="card-img-top" alt="">
                        <?php else: ?>
                            <img src="/static/images/no_image.png" class="card-img-top" alt="">
                        <?php endif; ?>
                        <div class="card-body">
                            <h5 class="card-title text-center"><?= $row['name'] ?></h5>
                            <p class="card-text"><?=$row['price']?> грн.</p>
                        </div>
                        <div class="card-body text-center">
                            <?php if (User::isAdmin()) : ?>
                                <a class="btn btn-primary" href="/product/edit/<?= $row['id']?>">Редагувати</a>
                                <a class="btn btn-danger" href="/product/delete/<?= $row['id']?>">Видалити</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    <?php else : ?>

        <?php if (!empty($rows)) : ?>

        <?php foreach ($rows as $row) : ?>
            <div class="col">
                <a href="/product/view/<?= $row['id'] ?>" class="card-link">
                    <div class="card">
                        <?php $filePath = 'files/product/'.$row['photo']; ?>
                        <?php if(is_file($filePath)) : ?>
                            <img src="/<?=$filePath ?>" class="card-img-top" alt="">
                        <?php else: ?>
                            <img src="/static/images/no_image.png" class="card-img-top" alt="">
                        <?php endif; ?>
                        <div class="card-body">
                            <h5 class="card-title text-center"><?= $row['name'] ?></h5>
                            <p class="card-text text-center"><?=$row['price']?> грн.</p>
                        </div>
                        <div class="card-body text-center">
                            <?php if (User::isAdmin()) : ?>
                                <a class="btn btn-primary" href="/product/edit/<?= $row['id']?>">Редагувати</a>
                                <a class="btn btn-danger" href="/product/delete/<?= $row['id']?>">Видалити</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
        <?php endif; ?>
    <?php endif; ?>
</div>


