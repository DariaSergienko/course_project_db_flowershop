<?php
/** @var array $product */
/** @var array $model */
/** @var array $errors */
/** @var array $categories */
core\Core::getInstance()->pageParams['title'] = 'Редагування товару';
?>
<h2 class="h3 mb-4 fw-normal text-center">Редагування товару</h2>
<form method="post" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">Назва товару</label>
        <input type="text" class="form-control" id="name" name="name" value="<?=$product['name']?>">
        <?php if(!empty($errors['name'])): ?>
            <div id="nameError" class="form-text text-danger"><?=$errors['name'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="category_id" class="form-label">Категорія товару</label>
        <select class="form-control" id="category_id" name="category_id">
            <?php foreach ($categories as $category) : ?>
                <option <?php if ($category['id'] == $product['category_id']) echo 'selected';?> value="<?=$category['id']?>"><?=$category['name']?></option>
            <?php endforeach; ?>
        </select>
        <?php if(!empty($errors['category_id'])): ?>
            <div id="categoryError" class="form-text text-danger"><?=$errors['category_id'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="price" class="form-label">Ціна товару</label>
        <input type="number" class="form-control" id="price" name="price" value="<?=$product['price']?>">
        <?php if(!empty($errors['price'])): ?>
            <div id="priceError" class="form-text text-danger"><?=$errors['price'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="count" class="form-label">Кількість одиниць товару</label>
        <input type="number" class="form-control" id="count" name="count" value="<?=$product['count']?>">
        <?php if(!empty($errors['count'])): ?>
            <div id="countError" class="form-text text-danger"><?=$errors['count'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="description" class="form-label">Опис товару</label>
        <textarea class="form-control ckeditor" id="description" name="description"><?=$product['description']?></textarea>
        <?php if(!empty($errors['description'])): ?>
            <div id="descriptionError" class="form-text text-danger"><?=$errors['description'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="visible" class="form-label">Чи відображати товар?</label>
        <select class="form-control" id="visible" name="visible">
            <?php if($product['visible'] == '1') :?>
                <option value="1" selected>Так</option>
                <option value="0">Ні</option>
            <?php else : ?>
                <option value="1">Так</option>
                <option value="0" selected>Ні</option>
            <?php endif; ?>
        </select>
        <?php if(!empty($errors['visible'])): ?>
            <div id="categoryError" class="form-text text-danger"><?=$errors['visible'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Файл з фотографією для товару</label>
        <input multiple type="file" class="form-control" id="file" name="file" accept="image/png">
    </div>

    <div>
        <button class="btn btn-primary">Зберегти</button>
    </div>
</form>
