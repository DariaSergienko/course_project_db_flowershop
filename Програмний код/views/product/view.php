<?php
/** @var array $product */
/** @var array $errors */
/** @var array $model */
/** @var array|null $reviews */
core\Core::getInstance()->pageParams['title'] = 'Перегляд товару';
?>
<?php
$user = \models\User::getCurrentAuthenticatedUser();
$product_reviews = \models\Review::getReviewsForProduct($product['id']);
?>

<h1 class="h3 mb-3 fw-normal text-center"><?= $product['name'] ?></h1>

<div class="container">
    <div class="row d-flex justify-content-between">
        <div class="col-6">
            <div class="container">
                <div class="row mb-3">
                    <div class="col-4">
                        Ціна товару:
                    </div>
                    <div class="col-8">
                        <strong><?= $product['price'] ?> грн.</strong>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-4">
                        Доступна кількість:
                    </div>
                    <div class="col-8">
                        <strong><?= $product['count'] ?> шт.</strong>
                    </div>
                </div>

                <form method="post" action="/product/purchase/<?= $product['id'] ?>">
                    <div class="row mb-3">
                        <div class="col-4">
                            Придбати:
                        </div>
                        <div class="col-3">
                            <input min="1" value="1" max="<?= $product['count'] ?>" class="form-control" type="number"
                                   name="count" id="count">
                        </div>
                        <?php if (!empty($errors['count'])): ?>
                            <div id="countError" class="form-text text-danger"><?= $errors['count']; ?></div>
                        <?php endif ?>

                    </div>
                    <div class="row mb-3">
                        <div class="col-4">

                        </div>
                        <div class="col-3">
                            <button class="btn-primary btn">Придбати</button>
                        </div>
                    </div>
                </form>

                <?php if (!empty($product['description'])) : ?>
                    <div class="row mb-3">
                        <div class="col-4">
                            <strong>Опис товару:</strong>
                        </div>
                        <div class="col-8">
                            <?= $product['description'] ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-4">
            <?php $filePath = 'files/product/' . $product['photo']; ?>
            <?php if (is_file($filePath)) : ?>
                <img style="width: 350px; height: 350px" src="/<?= $filePath ?>" class="img-thumbnail" alt="">
            <?php else: ?>
                <img style="width: 350px; height: 350px" src="/static/images/no_image.png" class="img-thumbnail" alt="">
            <?php endif; ?>
        </div>
    </div>

    <div class="container mb-3">
        <div class="mb-5">
            <h4>Загальний рейтинг товару</h4>
            <h1 class="text-warning mt-4 mb-4">
                <b><span id="average_rating"><?php
                        if (count($product_reviews) > 0) {
                            $avg_rating = 0;
                            $sum_rating = 0;
                            $count = 0;
                            foreach ($product_reviews as $review) {
                                $sum_rating += $review['user_rating'];
                                $count++;
                            }
                            round($avg_rating = $sum_rating / $count);
                            echo $avg_rating;
                        } else echo '0';
                        ?></span> / 5</b>
            </h1>
            <div class="mb-3">
                <i class="fas fa-star star-light mr-1 main_star <?php if ($avg_rating >= 1) echo 'text-warning' ?>"></i>
                <i class="fas fa-star star-light mr-1 main_star <?php if ($avg_rating >= 2) echo 'text-warning' ?>"></i>
                <i class="fas fa-star star-light mr-1 main_star <?php if ($avg_rating >= 3) echo 'text-warning' ?>"></i>
                <i class="fas fa-star star-light mr-1 main_star <?php if ($avg_rating >= 4) echo 'text-warning' ?>"></i>
                <i class="fas fa-star star-light mr-1 main_star <?php if ($avg_rating >= 5) echo 'text-warning' ?>"></i>
            </div>
        </div>
        <div>
            <?php if (\models\User::isUserAuthenticated()) : ?>
            <form method="post" action="/review/add/<?= $product['id'] ?>" style="text-align: right">
                <div class="d-flex justify-content-between align-items-center mt-3 mb-3">
                    <h4>Напишіть свій відгук тут :</h4>
                    <h5>
                        <button type="button" class="fas fa-star star-light submit_star mr-1 border-0 bg-transparent"
                                id="submit_star_1" data-rating="1"></button>
                        <button type="button" class="fas fa-star star-light submit_star mr-1 border-0 bg-transparent"
                                id="submit_star_2" data-rating="2"></button>
                        <button type="button" class="fas fa-star star-light submit_star mr-1 border-0 bg-transparent"
                                id="submit_star_3" data-rating="3"></button>
                        <button type="button" class="fas fa-star star-light submit_star mr-1 border-0 bg-transparent"
                                id="submit_star_4" data-rating="4"></button>
                        <button type="button" class="fas fa-star star-light submit_star mr-1 border-0 bg-transparent"
                                id="submit_star_5" data-rating="5"></button>
                    </h5>
                </div>
                <input type="hidden" id="for_star" name="star" value=" "/>
                <div class="form-group">
                    <textarea name="user_review" id="user_review" class="form-control"
                              placeholder="Ваш відгук..."></textarea>
                </div>
                <div class="form-group text-center mt-4">
                    <button class="btn btn-primary" id="save_review">Надіслати</button>
                </div>
            </form>
        </div>
    </div>

    <?php else : ?>
        <div class="mt-3 mb-3" style="text-align: right">Зареєструйтесь, щоб залишити відгук.</div>
    <?php endif; ?>
    <h3 class="mb-3">Коментарі</h3>
    <div>
        <?php if (!empty($product_reviews)) : ?>
            <?php foreach ($product_reviews as $review) : ?>
                <?php $user = \models\User::getUserById($review['user_id']) ?>
                <div>
                    <h5><?= $user['login'] ?></h5>
                    <h6 class="small">
                        <i class="fas fa-star star-light mr-1 main_star <?php if ($review['user_rating'] >= 1) echo 'text-warning' ?>"></i>
                        <i class="fas fa-star star-light mr-1 main_star <?php if ($review['user_rating'] >= 2) echo 'text-warning' ?>"></i>
                        <i class="fas fa-star star-light mr-1 main_star <?php if ($review['user_rating'] >= 3) echo 'text-warning' ?>"></i>
                        <i class="fas fa-star star-light mr-1 main_star <?php if ($review['user_rating'] >= 4) echo 'text-warning' ?>"></i>
                        <i class="fas fa-star star-light mr-1 main_star <?php if ($review['user_rating'] >= 5) echo 'text-warning' ?>"></i>
                    </h6>
                    <h6 class="small"><?= $review['date_and_time'] ?></h6>
                    <p><?= $review['user_review'] ?></p>
                    <hr>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <div>
                Відгуків про цей продукт ще немає...
            </div>
        <?php endif; ?>
    </div>
</div>

<script>
    $(document).ready(function () {

        let rating_data = 0;

        $(document).on('mouseenter', '.submit_star', function () {

            let rating = $(this).data('rating');

            reset_background();

            for (var count = 1; count <= rating; count++) {

                $('#submit_star_' + count).addClass('text-warning');

            }

        });

        function reset_background() {
            for (var count = 1; count <= 5; count++) {

                $('#submit_star_' + count).addClass('star-light');

                $('#submit_star_' + count).removeClass('text-warning');

            }
        }

        $(document).on('mouseleave', '.submit_star', function () {

            reset_background();

            for (var count = 1; count <= rating_data; count++) {

                $('#submit_star_' + count).removeClass('star-light');

                $('#submit_star_' + count).addClass('text-warning');
            }

        });

        $(document).on('click', '.submit_star', function () {

            rating_data = $(this).data('rating');
            document.getElementById('for_star').value = rating_data;
        });
    });
</script>