<?php
/** @var array $product */
core\Core::getInstance()->pageParams['title'] = 'Видалення товару';
?>

<div class="alert alert-warning" role="alert">
    <h4 class="alert-heading">Видалити товар "<?=$product['name']?>"?</h4>
    <hr>
    <p>
        <a href="/product/delete/<?=$product['id']?>/yes" class="btn btn-danger">Видалити</a>
        <a href="/product" class="btn btn-light">Відмінити</a>
    </p>
</div>
