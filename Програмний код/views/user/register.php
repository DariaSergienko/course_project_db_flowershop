<?php
/** @var array $errors */
/** @var array $model */
\core\Core::getInstance()->pageParams['title'] = 'Реєстрація на сайті';
?>
<h1 class="h3 mb-4 fw-normal text-center">Реєстрація</h1>

<main class="form-signin w-100 m-auto">
<form method="post" action="">
    <div class="mb-3">
        <label for="login" class="form-label">Логін</label>
        <input class="form-control" type="text" name="login" id="login" value="<?=$model['login']?>" aria-describedby="loginError"/>
        <?php if(!empty($errors['login'])): ?>
            <div id="loginError" class="form-text text-danger"><?=$errors['login'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Пароль</label>
        <input class="form-control" type="password" name="password" id="password" value="<?=$model['password']?>" aria-describedby="passwordError"/>
        <?php if(!empty($errors['password'])): ?>
            <div id="passwordError" class="form-text text-danger"><?=$errors['password'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="password2" class="form-label">Пароль (ще раз)</label>
        <input class="form-control" type="password" name="password2" id="password2" value="<?=$model['password2']?>" aria-describedby="password2Error"/>
        <?php if(!empty($errors['password2'])): ?>
            <div id="password2Error" class="form-text text-danger"><?=$errors['password2'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="lastname" class="form-label">Прізвище</label>
        <input class="form-control" type="text" name="lastname" id="lastname" value="<?=$model['lastname']?>" aria-describedby="lastnameError"/>
        <?php if(!empty($errors['lastname'])): ?>
            <div id="lastnameError" class="form-text text-danger"><?=$errors['lastname'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="firstname" class="form-label">Ім'я</label>
        <input class="form-control" type="text" name="firstname" id="firstname" value="<?=$model['firstname']?>" aria-describedby="firstnameError"/>
        <?php if(!empty($errors['firstname'])): ?>
            <div id="firstnameError" class="form-text text-danger"><?=$errors['firstname'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input class="form-control" type="email" name="email" id="email" value="<?=$model['email']?>" aria-describedby="emailError"/>
        <?php if(!empty($errors['email'])): ?>
            <div id="emailError" class="form-text text-danger"><?=$errors['email'];?></div>
        <?php endif ?>
    </div>
    <div class="mb-3">
        <label for="phoneNumber" class="form-label">Номер телефону</label>
        <input class="form-control" type="text" name="phoneNumber" id="phoneNumber" value="<?=$model['phoneNumber']?>" aria-describedby="phoneNumberError"/>
        <?php if(!empty($errors['phoneNumber'])): ?>
            <div id="phoneNumberError" class="form-text text-danger"><?=$errors['phoneNumber'];?></div>
        <?php endif ?>
    </div>
    <div>
        <button class="btn btn-primary">Зареєструватися</button>
    </div>
</form>
</main>