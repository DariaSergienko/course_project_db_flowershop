<?php
/** @var array $user */
/** @var array $orders */
/** @var array $users */
/** @var array $categories */
if(!\models\User::isAdmin())
    \core\Core::getInstance()->pageParams['title'] = 'Профіль користувача';
else
    \core\Core::getInstance()->pageParams['title'] = 'Перелік замовлень та статистика';
?>

<?php
if (\models\User::isAdmin()){
    $order_sum = 0;
    $max_category_name="";
    $max_category_count=-999;
    foreach ($orders as $order){
        $order_sum+=$order['totalPrice'];
    }
    $bought_flowers['category_name']['count']= [];
    foreach ($orders as $order) {
        $cart = \models\Order::getDecodedProductsArray($order['id']);
        foreach ($cart['products'] as $row){
            foreach ($categories as $category){
                if($row['product']['category_id'] == $category['id'])
                    $bought_flowers[$category['name']]['count']+=$row['count'];
            }
        }
    }
}
?>

   <?php if (!\models\User::isAdmin()) :?>
    <div class="d-flex justify-content-right mb-5">
        <div class="card">
            <div class="card-header"><strong>Ваші дані:</strong></div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Логін</strong></li>
                <li class="list-group-item"><strong>Ім'я</strong></li>
                <li class="list-group-item"><strong>Прізвище</strong></li>
                <li class="list-group-item"><strong>Телефон</strong></li>
                <li class="list-group-item"><strong>Email</strong></li>
            </ul>

        </div>
        <div class="card d-flex" style="width: 50%">
            <div class="card-header"><i class="invisible">`</i></div>
            <ul class="list-group list-group-flush ">
                <li class="list-group-item"><?= $user['login'] ?></li>
                <li class="list-group-item"><?= $user['firstname'] ?></li>
                <li class="list-group-item"><?= $user['lastname'] ?></li>
                <li class="list-group-item"><?= $user['phoneNumber'] ?></li>
                <li class="list-group-item"><?= $user['email'] ?></li>
            </ul>

        </div>
    </div>
    <?php endif; ?>


    <?php if (\models\User::isAdmin()) :  ?>
    <br><h1 class="h3 mb-4 fw-normal text-center "><strong>Cтатистика</strong></h1>
        <div class="container">
            <div class="container" style="display: flex; gap: 20px;">
                <div class="card d-flex text-center" style="width: 50%">
                    <div class="card-header"><i>Кількість користувачів</i></div>
                    <ul class="list-group list-group-flush ">
                        <li class="list-group-item"><?= sizeof($users)?></li>
                    </ul>
                </div>
                <div class="card d-flex text-center" style="width: 50%">
                    <div class="card-header"><i>Загальний прибуток</i></div>
                    <ul class="list-group list-group-flush ">
                        <li class="list-group-item"><?= $order_sum ?> грн.</li>
                    </ul>
                </div>
                <div class="card d-flex text-center" style="width: 50%">
                    <div class="card-header"><i>Кількість замовлень</i></div>
                    <ul class="list-group list-group-flush ">
                        <li class="list-group-item"><?= sizeof($orders) ?></li>
                    </ul>
                </div>
            </div>
            <h4 class="h6 mb-4 fw-normal text-center " style="margin-top: 20px"><strong>Cтатистика куплених квітів за категорією</strong></h4>
            <div class="container" style="display: flex; gap: 10px; ">
            <?php foreach ($categories as $category) :?>

                <div class="card d-flex text-center" style="width: 400px">
                    <div class="card-header"><i><?= $category['name']?></i></div>
                    <ul class="list-group list-group-flush ">
                        <?php if ($bought_flowers[$category['name']]['count']!= null) : ?>
                        <li class="list-group-item"><?= $bought_flowers[$category['name']]['count']?></li>
                        <?php else : ?>
                            <li class="list-group-item">0</li>
                        <?php endif; ?>

                    </ul>
                </div>

            <?php endforeach; ?>
            </div>
            <h4 class="h6 mb-4 fw-normal text-center " style="margin-top: 20px"><strong>Найпопулярніша категорія</strong></h4>
            <div class="container text-center align-middle" style="align-content: center; justify-content: center">
                <?php foreach ($categories as $category) :?>
                    <?php if ($bought_flowers[$category['name']]['count'] > $max_category_count) {
                        $max_category_count = $bought_flowers[$category['name']]['count'];
                        $max_category_name = $category['name'];
                    } ?>
                <?php endforeach; ?>
                <div class="card d-flex text-center" style="width: 400px">
                    <div class="card-header"><i><?= $max_category_name?></i></div>
                    <ul class="list-group list-group-flush ">
                        <?= $max_category_count?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>


    <div class="bg-transparent rounded-2 ">
<?php if (\models\User::isAdmin()) : ?>
    <br><h1 class="h3 mb-4 fw-normal text-center "><strong>Усі замовлення</strong></h1>
<?php else: ?>
    <br><h1 class="h3 mb-4 fw-normal text-center "><strong>Ваші замовлення</strong></h1>
<?php endif; ?>


    <?php if (!empty($orders)) : ?>
    <?php $index = 1;
    foreach ($orders as $order) : ?>
        <?php if (\models\User::isAdmin()) : ?>
        <div class="d-flex justify-content-between align-middle">
            <span class="m-2"> <strong>Id замовлення</strong> <?= $order['id'] ?> </span>
            <span class="m-2"><strong>Id користувача</strong> <?= $order['user_id'] ?> </span>
        </div>
        <?php endif; ?>
        <table class="table table-bordered">
            <thead class="table-secondary">
            <tr>
                <th>№</th>
                <th>Назва товару</th>
                <th>Вартість одиниці</th>
                <th>Кількість</th>
                <th>Загальна вартість</th>
                <th></th>
            </tr>
            </thead>

            <?php $index = 1;
            $cart = \models\Order::getDecodedProductsArray($order['id']);
            foreach ($cart['products'] as $row) : ?>
                <tr>
                    <td> <?= $index ?> </td>
                    <td> <?= $row['product']['name'] ?> </td>
                    <td> <?= $row['product']['price'] ?> грн.</td>
                    <td>
                        <?= $row['count'] ?>
                    </td>
                    <td> <?= $row['product']['price'] * $row['count'] ?></td>
                    <td></td>
                </tr>
                <?php $index++; endforeach; ?>
            <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <th>Загальна сума</th>
                <th><?= $cart['total_price'] ?></th>
            </tr>
            </tfoot>
        </table>
        <?php $index++; endforeach; ?>
    </div>
<?php else : ?>
    <div class="container mb-3">
        У Вас немає замовлень.
    </div>
<?php endif; ?>