<?php
/** @var string|null $error */
/** @var array $model */
\core\Core::getInstance()->pageParams['title'] = 'Вхід на сайт';
?>
<h1 class="h3 mb-4 fw-normal text-center">Вхід на сайт</h1>

<main class="form-signin w-100 m-auto">
<form action="" method="post">
    <?php if(!empty($error)): ?>
        <div class="message error text-center mb-2">
            <?=$error ?>
        </div>
    <?php endif; ?>
    <div class="form-floating mb-3">
        <input type="text" class="form-control" name="login" id="login" value="<?=$model['login']?>" placeholder="Логін">
        <label for="login">Логін</label>
    </div>
    <div class="form-floating  mb-3">
        <input type="password" class="form-control" name="password" id="password" value="<?=$model['password']?>" placeholder="Пароль">
        <label for="password">Пароль</label>
    </div>

    <button class="w-100 btn btn-lg btn-primary" type="submit">Увійти</button>
</form>
</main>