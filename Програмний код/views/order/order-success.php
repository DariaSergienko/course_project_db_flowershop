<?php
core\Core::getInstance()->pageParams['title'] = 'Успішне оформлення замовлення';
?>

<div class="alert alert-success" role="alert" >
    <b>Ваше замовлення було оформлене успішно.</b>
</div>
