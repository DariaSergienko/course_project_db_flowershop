<?php
use models\User;
core\Core::getInstance()->pageParams['title'] = 'Оформлення замовлення';
/** @var array $rows */
/** @var array $model */
/** @var array $errors */
/** @var array $cart */
?>

<h2 class="h3 mb-3 fw-normal text-center ">Оформлення замовлення</h2>
<div class="row g-3 d-flex justify-content-center">
    <div class="col-md-7 col-lg-6">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-black">Ваші дані</span>
        </h4>
        <form class="needs-validation"  method="post" novalidate="" action="/order/add">
            <div class="row g-3">
                <div class="col-12">
                    <label for="username" class="form-label">Логін</label>
                    <div class="input-group has-validation">
                        <input type="text" class="form-control" id="login" name="login" placeholder="login" required="" value="<?php
                        if(User::isUserAuthenticated()){
                            $user = User::getCurrentAuthenticatedUser();
                            echo "{$user['login']}";
                        }
                        ?>">
                    </div>
                    <?php if (!empty($errors['login'])): ?>

                        <div id="loginHelp" class="form-text text-danger"><?= $errors['login']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="col-sm-6">
                    <label for="firstname" class="form-label">Ім'я</label>
                    <input type="text" class="form-control" id="firstname"  name="firstname" placeholder="Ім'я" required="" value="<?php
                    if(User::isUserAuthenticated()){
                        $user = User::getCurrentAuthenticatedUser();
                        echo "{$user['firstname']}";
                    }
                    ?>">
                    <?php if (!empty($errors['firstname'])): ?>
                        <div id="firstnameHelp" class="form-text text-danger"> <?= $errors['firstname']; ?></div>
                    <?php endif; ?>
                </div>

                <div class="col-sm-6">
                    <label for="lastname" class="form-label">Прізвище</label>
                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Прізвище" required="" value="<?php
                    if(User::isUserAuthenticated()){
                        $user = User::getCurrentAuthenticatedUser();
                        echo "{$user['lastname']}";
                    }
                    ?>">
                    <?php if (!empty($errors['lastname'])): ?>
                        <div id="lastnameHelp" class="form-text text-danger"> <?= $errors['lastname']; ?></div>
                    <?php endif; ?>
                </div>

                <div class="col-12">
                    <label for="email" class="form-label">Електронна адреса</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="your_adress@gmail.com" value="<?php
                    if(User::isUserAuthenticated()){
                        $user = User::getCurrentAuthenticatedUser();
                        echo "{$user['email']}";
                    }
                    ?>">
                    <?php if (!empty($errors['email'])): ?>
                        <div id="emailHelp" class="form-text text-danger"> <?= $errors['email']; ?></div>
                    <?php endif; ?>
                </div>

                <div class="col-12">
                    <label for="phoneNumber" class="form-label">Номер телефону</label>
                    <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="0XXXXXXXXX" value="<?php
                    if(User::isUserAuthenticated()){
                        $user = User::getCurrentAuthenticatedUser();
                        echo "{$user['phoneNumber']}";
                    }
                    ?>">
                    <?php if (!empty($errors['phoneNumber'])): ?>
                        <div id="loginHelp" class="form-text text-danger"> <?= $errors['phoneNumber']; ?></div>
                    <?php endif; ?>
                </div>
            </div>

            <hr class="my-4">
            <div class="form-check mb-3">
                <input type="checkbox" class="form-check-input" id="check-register" name="check-register">
                <label class="form-check-label" for="check-register">Зареєструвати мене <span class="text-muted">(дозволяє переглядати оформлені замовлення)</span></label>
            </div>

            <div class="row g-3">
                <div class="mb-1 col-sm-6">
                    <label for="password" class="form-label">Пароль</label>
                    <input type="password" class="form-control" name="password" id="password" value="<?= $model['password'] ?>"  aria-describedby="passwordHelp"/>
                    <?php if (!empty($errors['password'])): ?>
                        <div id="passwordHelp" class="form-text text-danger"> <?= $errors['password']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="mb-1 col-sm-6">
                    <label for="password2" class="form-label">Пароль (ще раз)</label>
                    <input type="password" class="form-control" name="password2" id="password2" value="<?= $model['password2'] ?>"  aria-describedby="password2Help"/>
                    <?php if (!empty($errors['password2'])): ?>
                        <div id="password2Help" class="form-text text-danger"> <?= $errors['password2']; ?></div>
                    <?php endif; ?>
                </div>
            </div>


            <hr class="my-4">

            <h4 class="mb-3">Оплата замовлення</h4>

            <div class="my-3">
                <div class="form-check">
                    <input id="credit" name="paymentMethod" type="radio" class="form-check-input" value="credit" checked required="" onclick="showCard()">
                    <label class="form-check-label" for="credit">Кредитна карта</label>
                </div>
                <div class="form-check">
                    <input id="cash" name="paymentMethod" type="radio" class="form-check-input" value="cash" onclick="hideCard()">
                    <label class="form-check-label" for="cash">Готівкою при отриманні</label>
                </div>
            </div>

            <div class="row gy-3" id="cardInfo">

                <div class="col-md-6">
                    <label for="cc-number" class="form-label">Номер картки</label>
                    <input type="text" class="form-control" id="cc-number" name="cc-number" placeholder="0000 0000 0000 0000" required="">
                    <?php if (!empty($errors['cc-number'])): ?>
                        <div id="cc-number" class="form-text text-danger"> <?= $errors['cc-number']; ?></div>
                    <?php endif; ?>

                </div>

                <div class="col-md-3">
                    <label for="cc-expiration" class="form-label">Термін дії картки</label>
                    <input type="text" class="form-control" id="cc-expiration" name="cc-expiration" placeholder="02/25" required="">
                    <?php if (!empty($errors['cc-expiration'])): ?>
                        <div id="cc-expiration" class="form-text text-danger"> <?= $errors['cc-expiration']; ?></div>
                    <?php endif; ?>
                </div>

                <div class="col-md-3">
                    <label for="cc-cvv" class="form-label">CVV</label>
                    <input type="number" min="100" max="999" class="form-control" id="cc-cvv" name="cc-cvv" placeholder="123" required="">
                    <?php if (!empty($errors['cc-cvv'])): ?>
                        <div id="cc-cvv" class="form-text text-danger"> <?= $errors['cc-cvv']; ?></div>
                    <?php endif; ?>
                </div>
            </div>

            <hr class="my-4">
            <button class="w-100 btn btn-primary btn-lg" type="submit">Замовити</button>
        </form>
    </div>

    <div class="col-md-5 col-lg-4 order-md-last">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-black">Кошик</span>
            <span class="badge bg-secondary rounded-pill"><?=count($cart['products'])?></span>
        </h4>
        <ul class="list-group mb-3">

            <?php $index = 1;
            foreach ($cart['products'] as $row) :  ?>
                <li class="list-group-item d-flex justify-content-between lh-sm">
                    <div>
                        <h6 class="my-0"><?=$row['product']['name']?> <span class="text-muted">( <?=$row['count']?> )</span></h6>
                    </div>
                    <span class="text-muted"><?=$row['product']['price']?> грн.</span>
                </li>
                <?php $index++; endforeach; ?>

            <li class="list-group-item d-flex justify-content-between">
                <span>Загально</span>
                <strong><?=$cart['total_price']?> грн.</strong>
            </li>
        </ul>
    </div>
</div>

<script>
    let cash = document.getElementById('cash');
    let credit = document.getElementById('credit');
    let card_part = document.getElementById('cardInfo');
    function hideCard(){
            card_part.style.visibility = 'hidden';
    }
    function showCard(){
            card_part.style.visibility = 'visible';
    }
</script>
