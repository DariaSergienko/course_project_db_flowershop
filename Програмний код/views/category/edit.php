<?php
core\Core::getInstance()->pageParams['title'] = 'Редагування категорії';
/** @var array $category */
/** @var array $model */
/** @var array $errors */
?>
<h2 class="h3 mb-4 fw-normal text-center">Редагування категорії</h2>
<form method="post" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">Назва категорії</label>
        <input type="text" class="form-control" id="name" name="name" value="<?=$category['name']?>">
        <?php if(!empty($errors['name'])): ?>
            <div id="nameError" class="form-text text-danger"><?=$errors['name'];?></div>
        <?php endif ?>
    </div>
    <div class="col-3">
    <?php $filePath = 'files/category/'.$category['photo']; ?>
    <?php if(is_file($filePath)) : ?>
        <img src="/<?=$filePath ?>" class="card-img-top img-thumbnail" alt="">
    <?php else: ?>
        <img src="/static/images/no_image.png" class="card-img-top img-thumbnail" alt="">
    <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Файл з фотографією для категорії (замінити фото)</label>
        <input type="file" class="form-control" id="file" name="file" accept="image/png">
    </div>
    <div>
        <button class="btn btn-primary">Зберегти</button>
    </div>
</form>