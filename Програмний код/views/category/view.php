<?php
/** @var array $category */
/** @var array $products */
use models\User;
core\Core::getInstance()->pageParams['title'] = 'Перегляд категорії';
?>

<h2 class="h3 mb-4 fw-normal text-center"><?=$category['name']?></h2>
<?php if(User::isAdmin()) :?>
<div class="d-flex justify-content-center">
    <a href="/product/add/<?=$category['id']?>" class="btn btn-success mb-3">Додати товар</a>
</div>
<?php endif; ?>
<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach ($products as $row) : ?>
        <div class="col">
            <a href="/product/view/<?= $row['id'] ?>" class="card-link">
                <div class="card">
                    <?php $filePath = 'files/product/'.$row['photo']; ?>
                    <?php if(is_file($filePath)) : ?>
                        <img src="/<?=$filePath ?>" class="card-img-top" alt="">
                    <?php else: ?>
                        <img src="/static/images/no_image.png" class="card-img-top" alt="">
                    <?php endif; ?>
                    <div class="card-body">
                        <h5 class="card-title text-center"><?= $row['name'] ?></h5>
                        <p class="card-text text-center"><?=$row['price']?> грн.</p>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
