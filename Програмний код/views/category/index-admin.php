<?php
/** @var array $rows */
core\Core::getInstance()->pageParams['title'] = 'Категорії';
use models\User;

?>
<h2 class="h3 mb-4 fw-normal text-center">Список категорій</h2>
<?php if (User::isAdmin()) : ?>
    <div class=" d-flex justify-content-center">
        <a href="/category/add" class=" btn btn-success mb-3 text-center text-white">Додати категорію</a>
    </div>
<?php endif; ?>
<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach ($rows as $row) : ?>
        <div class="col">
            <a href="/category/view/<?= $row['id'] ?>" class="card-link">
                <div class="card">
                    <?php $filePath = 'files/category/' . $row['photo']; ?>
                    <?php if (is_file($filePath)) : ?>
                        <img src="/<?= $filePath ?>" class="card-img-top" alt="">
                    <?php else: ?>
                        <img src="/static/images/no_image.png" class="card-img-top" alt="">
                    <?php endif; ?>
                    <div class="card-body">
                        <h5 class="card-title text-center"><?= $row['name'] ?></h5>
                        <!--        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>-->
                    </div>
                    <div class="card-body text-center">
                        <a class="btn btn-primary" href="/category/edit/<?= $row['id'] ?>">Редагувати</a>
                        <a class="btn btn-danger" href="/category/delete/<?= $row['id'] ?>">Видалити</a>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>