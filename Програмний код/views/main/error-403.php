<?php
use core\Core;

Core::getInstance()->pageParams['title'] = 'Помилка 403';
?>

<div class="alert alert-danger" role="alert">
    <div><b>Помилка 403.</b> Доступ заборонено!</div>
</div>
