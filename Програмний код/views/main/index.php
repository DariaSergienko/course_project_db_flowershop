<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Головна сторінка</title>
</head>
<body class=" h-100 text-center bg-white main_page">

<div class="cover-container d-flex w-100 h-100 p-5 mx-auto flex-column mb-4 bg-secondary rounded-4">
    <main class="px-3">
        <h1 class="text-white">Головна сторінка</h1>
        <p class="lead text-white">У нашому магазині Ви зможете придбати гарні квіти або букети на будь-який смак.</p>
        <p class="lead mt-4">
            <a href="http://cms/category" class="btn btn-lg btn-light  bg-light">Придбати квіти</a>
        </p>
    </main>
</div>
</body>
</html>