<?php

namespace models;

use core\Core;

class Order
{
    protected static string $tableName = 'orders';

    public static function addOrder($user_id, $products, $firstname, $lastname, $phoneNumber, $email, $totalPrice){
        Core::getInstance()->db->insert(self::$tableName,
            [
                'user_id' => $user_id,
                'products' => $products,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'phoneNumber' => $phoneNumber,
                'email' => $email,
                'totalPrice' => $totalPrice
            ]);
    }

    public static function getOrderByID($id){
        $row = Core::getInstance()->db->select(self::$tableName,"*", ['id' => $id]);
        if(!empty($row))
            return $row[0];
        else
            return null;
    }

    public static function getDecodedProductsArray($order_id){
        $cart = self::getOrderByID($order_id);
        $products = $cart['products'];
        $json = json_decode($products, true);
        if(is_array($json)){
            return $json;
        }
        else
            return null;
//        $product_id = 3;
//        var_dump($json['products']["{$product_id}"]['product']['name']);
    }

    public static function deleteOrder($id){
        Core::getInstance()->db->delete(self::$tableName, ['id' => $id]);
    }

    public static function getOrdersByUserId($user_id){
        return Core::getInstance()->db->select(self::$tableName,"*", ['user_id' => $user_id]);
    }

    public static function getOrdersAdmin(){
        return Core::getInstance()->db->select(self::$tableName);
    }
}