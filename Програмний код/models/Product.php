<?php

namespace models;

use core\Core;
use core\Utils;

class Product
{
    protected static $tableName = 'product';

    public static function addProduct($row, $photoPath)
    {
        do {
            $fileName = uniqid() . '.png';
            $newPath = "files/product/{$fileName}";
        }
        while(file_exists($newPath));
        move_uploaded_file($photoPath,$newPath);
        $row['photo'] = $fileName;
        $fieldsList = ['name', 'category_id', 'price', 'count', 'description', 'visible','photo'];
        $row = Utils::filterArray($row, $fieldsList);
        Core::getInstance()->db->insert(self::$tableName, $row);
    }

    public static function deleteProduct($id){
        self::deletePhotoFile($id);
        Core::getInstance()->db->delete(self::$tableName,[
            'id' => $id
        ]);
    }

    public static function updateProduct($id, $row){
        $fieldsList = ['name', 'category_id', 'price', 'count', 'description', 'visible', 'photo'];
        $row = Utils::filterArray($row, $fieldsList);
        Core::getInstance()->db->update(self::$tableName, $row, [
            'id' => $id
        ]);
    }

    public static function getProductById($id){
        $row = Core::getInstance()->db->select(self::$tableName,"*",[
            'id' => $id
        ]);
        if(!empty($row))
            return $row[0];
        else
            return null;
    }

    public static function getProductsInCategory($category_id){
        $rows = Core::getInstance()->db->select(self::$tableName,"*",[
            'category_id' => $category_id
        ]);
        return $rows;
    }

    public static function getProducts(){
        $rows = Core::getInstance()->db->select(self::$tableName);
        return $rows;
    }

    public static function changePhoto($id, $newPhoto){
        self::deletePhotoFile($id);
        do {
            $fileName = uniqid() . '.png';
            $newPath = "files/product/{$fileName}";
        }
        while(file_exists($newPath));
        move_uploaded_file($newPhoto,$newPath);
        Core::getInstance()->db->update(self::$tableName,[
            'photo' => $fileName
        ],[
            'id' => $id
        ]);
    }

    public static function deletePhotoFile($id){
        $row = self::getProductById($id);
        $photoPath = 'files/product/'.$row['photo'];
        if(is_file($photoPath))
            unlink($photoPath);
    }

    public static function searchProductsByName($name){
        $rows = Core::getInstance()->db->selectAlt(self::$tableName,'*',[
            'name' => "%{$name}%",
            'description' => "%{$name}%"
        ]);
        return $rows;
    }

    public static function searchProductsByPrice($min=0, $max=10000){
        if($min > $max){
            $temp = $min;
            $min = $max;
            $max = $temp;
        }
        $rows = Core::getInstance()->db->selectFilterForPrice(self::$tableName,'*',[
            'price' => "{$min} AND {$max}"
        ]);
        return $rows;
    }
}