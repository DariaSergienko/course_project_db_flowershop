<?php

namespace models;

use core\Core;

class Review
{
    protected static $tableName = 'reviews';

    public static function addReview($user_id,$product_id,$user_rating,$user_review,$date_and_time){
        Core::getInstance()->db->insert(self::$tableName,[
            'user_id' => $user_id,
            'product_id' => $product_id,
            'user_rating' => $user_rating,
            'user_review' => $user_review,
            'date_and_time' => $date_and_time
        ]);
    }

    public static function getReviewById($id){
        $rows = Core::getInstance()->db->select(self::$tableName,"*",[
            'id' => $id
        ]);
        if(!empty($rows))
            return $rows[0];
        else
            return null;
    }

    public static function getReviews(){
        $rows = Core::getInstance()->db->select(self::$tableName);
        return $rows;
    }

    public  static  function  getReviewsForProduct($product_id){
        $rows = Core::getInstance()->db->select(self::$tableName, '*',[
            'product_id' => $product_id
        ]);
        return $rows;
    }
}