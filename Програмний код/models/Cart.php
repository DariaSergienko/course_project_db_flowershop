<?php

namespace models;

class Cart
{
    public static function addProduct($product_id,$count = 1){
        if(!is_array($_SESSION['cart']))
            $_SESSION['cart'] = [];
        $_SESSION['cart'][$product_id] += $count;
    }

    public static function changeProductCount($product_id,$new_count){
        foreach ($_SESSION['cart']['products'] as $product){
            if ($product['product']['id'] == $product_id)
                $product['count'] = $new_count;

        }
    }

    public static function removeProduct($product_id){
        unset($_SESSION['cart'][$product_id]);
    }

    public static function getProductsInCart(){
        if(is_array($_SESSION['cart'])){
            $result = [];
            $products = [];
            $totalPrice = 0;
            foreach ($_SESSION['cart'] as $product_id => $count){
                $product = Product::getProductById($product_id);
                $totalPrice += $product['price'] * $count;
                $products[] = ['product' => $product, 'count' => $count];
            }
            $result['products'] = $products;
            $result['total_price'] = $totalPrice;
            return $result;
        }
        return null;
    }

    public static function clearCart(){
        unset($_SESSION['cart']);
    }


}