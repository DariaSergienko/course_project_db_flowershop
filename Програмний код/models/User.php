<?php

namespace models;

use core\Core;
use core\Utils;

class User
{
    protected static $tableName = 'user';

    public static function addUser($login, $password, $lastname, $firstname, $email, $phoneNumber)
    {
        \core\Core::getInstance()->db->insert(
            self::$tableName, [
                'login' => $login,
                'password' => self:: hashPassword($password),
                'lastname' => $lastname,
                'firstname' => $firstname,
                'email' => $email,
                'phoneNumber' => $phoneNumber
            ]
        );
    }

    public static function updateUser($id, $updatesArray)
    {
        $updatesArray = Utils::filterArray($updatesArray, ['lastname','firstname']);
        \core\Core::getInstance()->db->update(self::$tableName, $updatesArray, [
            'id' => $id
        ]);
    }

    public static function isLoginExists($login){
        $user = \core\Core::getInstance()->db->select(self::$tableName,'*',[
            'login' => $login
        ]);
        return !empty($user);
    }

    public static function isEmailExists($email){
        $user = \core\Core::getInstance()->db->select(self::$tableName,'*',[
            'email' => $email
        ]);
        return !empty($user);
    }

    public static function verifyUser($login,$password){
        $user = \core\Core::getInstance()->db->select(self::$tableName,'*',[
            'login' => $login,
            'password' => $password
        ]);
        return !empty($user);
    }

    public static function getUserByLoginAndPassword($login, $password){
        $user = \core\Core::getInstance()->db->select(self::$tableName,'*',[
            'login' => $login,
            'password' => self::hashPassword($password)
        ]);
        if(!empty($user))
            return $user[0];
        return null;
    }

    public static function getUserById($id){
        $rows = Core::getInstance()->db->select(self::$tableName,"*",[
            'id' => $id
        ]);
        if(!empty($rows))
            return $rows[0];
        else
            return null;
    }

    public static function getUsersAdmin(){
        return Core::getInstance()->db->select(self::$tableName);
    }


    public static function authenticateUser($user){
        $_SESSION['user'] = $user;
    }

    public static function logoutUser(){
        unset($_SESSION['cart']);
        unset($_SESSION['user']);
    }

    public static function isUserAuthenticated(){
        return isset($_SESSION['user']);
    }

    public static function getCurrentAuthenticatedUser(){
        return $_SESSION['user'];
    }

    public static function isAdmin(){
        $user = self::getCurrentAuthenticatedUser();
        return $user['access_level'] == 10;
    }

    public static function hashPassword($password){
        return md5($password);
    }
}