<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Cart;
use models\Category;
use models\Product;
use models\User;

class ProductController extends Controller
{
    public function indexAction()
    {
        $rows = Product::getProducts();
        return $this->render(null, [
            'rows' => $rows
        ]);
    }

    public function addAction($params)
    {
        $category_id = intval($params[0]);
        if (empty($category_id))
            $category_id = null;
        $categories = Category::getCategories();
        if (!User::isAdmin())
            return $this->error(403);
        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            $_POST['name'] = trim($_POST['name']);
            if (empty($_POST['name']))
                $errors['name'] = "Ім'я товару не може бути порожнім";
            if (empty(trim($_POST['category_id'])))
                $errors['category_id'] = 'Дане поле не може бути порожнім';
            if (empty(trim($_POST['price'])))
                $errors['price'] = 'Дане поле не може бути порожнім';
            if ($_POST['price'] <= 0)
                $errors['price'] = 'Ціна вказана некоректно';
            if ($_POST['count'] <= 0)
                $errors['count'] = 'Кількість товарів вказана некоректно';
            if (empty(trim($_POST['count'])))
                $errors['count'] = 'Дане поле не може бути порожнім';
            if (empty($errors)) {
                Product::addProduct($_POST, $_FILES['file']['tmp_name']);
                return $this->redirect('/product');
            } else {
                $model = $_POST;
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model,
                    'categories' => $categories,
                    'category_id' => $category_id
                ]);
            }
        }
        return $this->render(null,
            [
                'categories' => $categories,
                'category_id' => $category_id
            ]);
    }

    public function editAction($params)
    {
        $categories = Category::getCategories();
        $id = intval($params[0]);
        if (!User::isAdmin())
            return $this->error(403);
        if ($id > 0) {
            $product = Product::getProductById($id);
            if (Core::getInstance()->requestMethod === 'POST') {
                $errors = [];
                $_POST['name'] = trim($_POST['name']);
                if (empty($_POST['name']))
                    $errors['name'] = "Ім'я товару не може бути порожнім";
                if (empty(trim($_POST['category_id'])))
                    $errors['category_id'] = 'Дане поле не може бути порожнім';
                if (empty(trim($_POST['price'])))
                    $errors['price'] = 'Дане поле не може бути порожнім';
                if ($_POST['price'] <= 0)
                    $errors['price'] = 'Ціна вказана некоректно';
                if ($_POST['count'] <= 0)
                    $errors['count'] = 'Кількість товарів вказана некоректно';
                if (empty(trim($_POST['count'])))
                    $errors['count'] = 'Дане поле не може бути порожнім';
                if (empty($errors)) {
                    Product::updateProduct($id, $_POST);
                    if (!empty($_FILES['file']['tmp_name']))
                        Product::changePhoto($id, $_FILES['file']['tmp_name']);
                    return $this->redirect('/product/index');
                }
                else {
                    $model = $_POST;
                    return $this->render(null, [
                        'errors' => $errors,
                        'model' => $model,
                        'product' => $product,
                        'categories' => $categories,

                    ]);
                }

            }
            return $this->render(null, [
                'product' => $product,
                'categories' => $categories,
            ]);
        } else
            return $this->error(403);
    }

    public function deleteAction($params)
    {
        $id = intval($params[0]);
        $yes = boolval($params[1] === 'yes');
        if (!User::isAdmin())
            return $this->error(403);
        if ($id > 0) {
            $product = Product::getProductById($id);
            if($yes){
                $filePath = 'files/product/'.$product['photo'];
                if (is_file($filePath))
                    unlink($filePath);
                Product::deleteProduct($id);
                return $this->redirect('/product');
            }
            return $this->render(null, [
                'product' => $product
            ]);
        } else
            return $this->error(403);
    }

    public function viewAction($params){
        $id = intval($params[0]);
        $product = Product::getProductById($id);
        return $this->render(null, [
            'product' => $product
        ]);
    }

    public function purchaseAction($params){
        $id = intval($params[0]);
        $product = Product::getProductById($id);

        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            if (empty($_POST['count']))
                $errors['count'] = "Це поле не може бути порожнім";
            if ($_POST['count']<=0)
                $errors['count'] = "Мінімальна кількість товару - 1";
            if ($_POST['count']>$product['count'])
                $errors['count'] = "На складі немає такої кількості товарів";
            if (empty($errors)) {
                Cart::addProduct($id, $_POST['count']);

                $this->renderViewAlt('product/view',
                    [
                        'product' => $product
                    ]);
                $this->redirect("/product/view/{$product['id']}");
            } else {
                $model = $_POST;
                $this->renderViewAlt('product/view', [
                    'errors' => $errors,
                    'model' => $model,
                    'product' => $product
                ]);
                $this->redirect("/product/view/{$product['id']}");
            }
        }
        $this->renderViewAlt('product/view',
            [
                'product' => $product
            ]);
        $this->redirect("/product/view/{$product['id']}");
    }

    public function searchAction(){
        if(Core::getInstance()->requestMethod === 'POST'){
            $found_products = Product::searchProductsByName($_POST['search_statement']);
            return $this->renderViewAlt('product/index',
                [
                    'found_products' => $found_products
                ]);
        }
    }

    public function filterAction(){
        if(Core::getInstance()->requestMethod === 'POST'){
            $found_products = Product::searchProductsByPrice($_POST['min_price'],$_POST['max_price']);
            return $this->renderViewAlt('product/index',
                [
                    'found_products' => $found_products
                ]);
        }
    }

}