<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Category;
use models\Order;
use models\User;

class UserController extends Controller
{
    public function indexAction(){
        echo "User Index Action";
    }

    public function registerAction(){
        if(User::isUserAuthenticated())
            $this->redirect('/');
        if(Core::getInstance()->requestMethod === 'POST'){
            $errors = [];
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                $errors['email'] = 'Помилка при введенні електронної пошти';
            if(User::isEmailExists($_POST['email']))
                $errors['email'] = 'Даний email зайнято';

            if(User::isLoginExists($_POST['login']))
                $errors['login'] = 'Даний логін зайнято';
            if(empty(trim($_POST['login'])))
                $errors['login'] = 'Дане поле не може бути порожнім';

            if($_POST['password'] != $_POST['password2'])
                $errors['password'] = 'Паролі не співпадають';
            if(empty(trim($_POST['password'])))
                $errors['password'] = 'Дане поле не може бути порожнім';
            if(empty(trim($_POST['password2'])))
                $errors['password2'] = 'Дане поле не може бути порожнім';

            if(empty(trim($_POST['lastname'])))
                $errors['lastname'] = 'Дане поле не може бути порожнім';
            if(empty(trim($_POST['firstname'])))
                $errors['firstname'] = 'Дане поле не може бути порожнім';

            if(empty(trim($_POST['phoneNumber'])))
                $errors['phoneNumber'] = 'Дане поле не може бути порожнім';
//            if(!preg_match('/^\+380\d{3}\d{2}\d{2}\d{2}$/', $_POST['phoneNumber']))
//                $errors['phoneNumber'] = 'Номер телефону введено некоректно';
            if(!preg_match('/^[0-9]{10}+$/', $_POST['phoneNumber']))
                $errors['phoneNumber'] = 'Номер телефону введено некоректно';

            if(count($errors) > 0) {
                $model = $_POST;
                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model
                ]);
            } else{ /* якщо нема помилок додаєм користувача */
                User::addUser($_POST['login'], $_POST['password'],$_POST['lastname'],$_POST['firstname'],$_POST['email'],$_POST['phoneNumber']);
                $user = User::getUserByLoginAndPassword($_POST['login'],$_POST['password']);
                User::authenticateUser($user);
                return $this->renderView('register-success');
            }
        } else
        return $this->render();
    }

    public function loginAction(){
        if(User::isUserAuthenticated())
            $this->redirect('/');
        if(Core::getInstance()->requestMethod === 'POST') {
            $user = User::getUserByLoginAndPassword($_POST['login'], $_POST['password']);
            $error = null;
            if (empty($user)) {
                $error = 'Неправильний логін або пароль';
            } else {
                User::authenticateUser($user);
                $this->redirect('/');
            }
        }
        return $this->render(null,[
            'error' => $error
        ]);
    }

    public function logoutAction(){
        User::logoutUser();
        $this->redirect('/user/login');
    }

    public function profileAction(){
        $user = User::getCurrentAuthenticatedUser();
        if(User::isAdmin()){
            $users = User::getUsersAdmin();
            $orders = Order::getOrdersAdmin();
            $categories = Category::getCategories();
        }
        else
            $orders = Order::getOrdersByUserId($user['id']);
        return $this->render(null,
            [
                'user' => $user,
                'orders' => $orders,
                'users' => $users,
                'categories'=> $categories
            ]);
    }

}