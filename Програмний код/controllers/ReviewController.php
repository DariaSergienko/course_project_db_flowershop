<?php

namespace controllers;

use Cassandra\Date;
use core\Core;
use models\Product;
use models\Review;
use models\User;

class ReviewController extends \core\Controller
{
    public function addAction($params){
        $reviews = Review::getReviews();
        $user = User::getCurrentAuthenticatedUser();
        $id = intval($params[0]);
        $product = Product::getProductById($id);
        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            $_POST['user_review'] = trim($_POST['user_review']);
            if(empty($_POST['user_review']))
                $errors['user_review'] = "Дане поле не може бути порожнім";
            if (empty($errors)) {
                $today = date("Y.m.d");
                Review::addReview($user['id'],$product['id'],$_POST['star'],$_POST['user_review'],$today);
                $reviews = Review::getReviews();
                $this->renderViewAlt('product/view',
                    [
                        'product' => $product,
                        'reviews' => $reviews
                    ]);
                $this->redirect("/product/view/{$product['id']}");
            }
            else {
                $model = $_POST;
                 $this->renderViewAlt('product/view', [
                    'errors' => $errors,
                    'model' => $model,
                    'product' => $product,
                    'reviews' => $reviews
                ]);
                $this->redirect("/product/view/{$product['id']}");
            }
        }
         $this->renderViewAlt('product/view',[
            'product' => $product,
            'reviews' => $reviews
        ]);
        $this->redirect("/product/view/{$product['id']}");
    }




}