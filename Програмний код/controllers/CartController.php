<?php

namespace controllers;

use core\Controller;
use models\Cart;
use models\Product;

class CartController extends Controller
{
    public function indexAction(){
        $cart = Cart::getProductsInCart();
        return $this->render(null,[
            'cart' => $cart
        ]);
    }

    public function clearAction(){
        Cart::clearCart();
        $this->redirect('/cart');
    }

    public function removeAction($params){
        $id = intval($params[0]);
        Cart::removeProduct($id);
        if(empty($_SESSION['cart']))
            unset($_SESSION['cart']);
        $this->redirect('/cart');
    }

    public function changeAction(){
        $this->render();
    }

}