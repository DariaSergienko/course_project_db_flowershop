<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Cart;
use models\Order;
use models\Product;
use models\User;

class OrderController extends Controller
{

    public function indexAction()
    {
        $cart = Cart::getProductsInCart();
        return $this->render(null, [
            'cart' => $cart
        ]);
    }

    public function addAction()
    {
        $cart = Cart::getProductsInCart();
        $json = json_encode($cart);

        if (Core::getInstance()->requestMethod === 'POST') {

            $errors = [];

            if (empty(trim($_POST['login'])))
                $errors['login'] = "Дане поле не може бути порожнім";
            if ($_POST['check-register'] == 'on' && User::isLoginExists($_POST['login']))
                $errors['login'] = 'Користувач з таким логіном вже існує';

            if (empty(trim($_POST['firstname'])))
                $errors['firstname'] = "Дане поле не може бути порожнім";
            if (empty(trim($_POST['lastname'])))
                $errors['lastname'] = "Дане поле не може бути порожнім";

            if (empty(trim($_POST['email'])))
                $errors['email'] = "Дане поле не може бути порожнім";
            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                $errors['email'] = 'Помилка при введені електроної пошти';

            if (empty(trim($_POST['phoneNumber'])))
                $errors['phoneNumber'] = "Дане поле не може бути порожнім";
            if (!preg_match('/^[0-9]{10}+$/', $_POST['phoneNumber']))
                $errors['phoneNumber'] = 'Введіть номер в форматі 0XXXXXXXXX';

            if ($_POST['paymentMethod'] == "credit") {
                if (empty(trim($_POST['cc-number'])))
                    $errors['cc-number'] = "Дане поле не може бути порожнім";
                if (!preg_match('/^4[0-9]{12}(?:[0-9]{3})?$/', trim($_POST['cc-number'])))
                    $errors['cc-number'] = "Номер картки повинен мати 16 цифр";

                if (empty(trim($_POST['cc-expiration'])))
                    $errors['cc-expiration'] = "Дане поле не може бути порожнім";
                if (!preg_match("/(0[1-9]|1[0-2])\/[0-9]{2}$/", $_POST['cc-expiration']))
                    $errors['cc-expiration'] = "Введіть дату в форматі 12/25";

                if (empty(trim($_POST['cc-cvv'])))
                    $errors['cc-cvv'] = "Дане поле не може бути порожнім";
                if (strlen($_POST['cc-cvv'])!=3 )
                    $errors['cc-cvv'] = "CVV має складатись з трьох цифр";
            }

            if (count($errors) > 0) {
                $model = $_POST;
                return $this->render('views/order/index.php', [
                        'errors' => $errors,
                        'model' => $model,
                        'cart' => $cart
                    ]
                );
            } else {

                if (User::isUserAuthenticated()) {
                    $user = User::getCurrentAuthenticatedUser();
                    Order::addOrder($user['id'], $json, $_POST['firstname'], $_POST['lastname'], $_POST['phoneNumber'], $_POST['email'], $cart['total_price']);
                } elseif ($_POST['check-register'] == 'on') {

                    User::addUser($_POST['login'], $_POST['password'], $_POST['firstname'], $_POST['lastname'], $_POST['phoneNumber'], $_POST['email']);
                    $user = User::getUserByLoginAndPassword($_POST['login'], $_POST['password']);
                    User::authenticateUser($user);
                    Order::addOrder($user['id'], $json, $_POST['firstname'], $_POST['lastname'], $_POST['phoneNumber'], $_POST['email'], $cart['total_price']);
                } else {

                    Order::addOrder(null, $json, $_POST['firstname'], $_POST['lastname'], $_POST['phoneNumber'], $_POST['email'], $cart['total_price']);
                }

                foreach ($cart['products'] as $product) {
                    $new_count = $product['product']['count'] - $product['count'];
                    Product::updateProduct($product['product']['id'], [
                        'count' => $new_count
                    ]);
                }

                Cart::clearCart();
                return $this->render('views/order/order-success.php');
            }
        }
        return $this->render();
    }

    public function deleteAction($params)
    {
        $id = intval($params[0]);
        $yes = boolval($params[1] === 'yes');

        if (!User::isAdmin())
            return $this->error(403);
        if ($id > 0) {
            if ($yes) {
                Order::deleteOrder($id);
                return $this->redirect('/order');
            }
            return $this->render();
        } else
            return $this->error(403);
    }

}