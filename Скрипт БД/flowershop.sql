-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Чрв 12 2023 р., 22:56
-- Версія сервера: 8.0.30
-- Версія PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `flowershop`
--

-- --------------------------------------------------------

--
-- Структура таблиці `category`
--

CREATE TABLE `category` (
  `id` int NOT NULL COMMENT 'ID категорії',
  `name` varchar(255) NOT NULL COMMENT 'Назва категорії',
  `photo` varchar(255) NOT NULL COMMENT 'Назва фотографії'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп даних таблиці `category`
--

INSERT INTO `category` (`id`, `name`, `photo`) VALUES
(1, 'Троянди', '63c56325966b9.png'),
(2, 'Хризантеми', '63c5637a30def.png'),
(3, 'Гербери', '63c5643c48e74.png'),
(7, 'Тюльпани', '63c567240e48d.png'),
(8, 'Іриси', '63c5684d9c818.png'),
(9, 'Орхідеї', '63c56942744bf.png'),
(10, 'Троянди кущові', '63c569c98ec28.png'),
(11, 'Еустоми', '63c56a0bed7c2.png'),
(15, 'Альстромерії', '63c56acb44c83.png'),
(16, 'Збірні букети', '63c56b5cc897f.png'),
(17, 'Елітні букети', '63c56b9a25c6f.png');

-- --------------------------------------------------------

--
-- Структура таблиці `orders`
--

CREATE TABLE `orders` (
  `id` int NOT NULL COMMENT 'ID',
  `user_id` int DEFAULT NULL COMMENT 'ID користувача',
  `firstname` varchar(255) NOT NULL COMMENT 'Ім''я',
  `lastname` varchar(255) NOT NULL COMMENT 'Прізвище',
  `phoneNumber` varchar(255) NOT NULL COMMENT 'Номер телефону',
  `email` varchar(255) NOT NULL COMMENT 'Email',
  `products` text NOT NULL COMMENT 'Замовлені товари',
  `totalPrice` bigint NOT NULL COMMENT 'Вартість замовлення'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп даних таблиці `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `firstname`, `lastname`, `phoneNumber`, `email`, `products`, `totalPrice`) VALUES
(4, 2, 'Human', 'Humanoid', '0672853958', 'helloimhuman@gmail.com', '{\"products\":[{\"product\":{\"id\":\"17\",\"name\":\"\\u0411\\u0443\\u043a\\u0435\\u0442 \\\"\\u042f\\u0441\\u043a\\u0440\\u0430\\u0432\\u0456 \\u0441\\u043e\\u043d\\u0435\\u0447\\u043a\\u0430\\\"\",\"category_id\":\"2\",\"price\":\"1000\",\"count\":\"12\",\"description\":\"<p>\\u042f\\u0441\\u043d\\u0456 \\u0441\\u043e\\u043d\\u0435\\u0447\\u043a\\u0430 \\u0445\\u0440\\u0438\\u0437\\u0430\\u043d\\u0442\\u0435\\u043c \\u043f\\u043e\\u0440\\u0430\\u0434\\u0443\\u044e\\u0442\\u044c \\u0434\\u043e\\u0440\\u043e\\u0433\\u043e\\u0433\\u043e \\u043e\\u0434\\u0435\\u0440\\u0436\\u0443\\u0432\\u0430\\u0447\\u0430 \\u0456 \\u043f\\u043e\\u0434\\u0430\\u0440\\u0443\\u044e\\u0442\\u044c \\u043f\\u043e\\u0441\\u043c\\u0456\\u0448\\u043a\\u0443 \\u043d\\u0430 \\u043a\\u043e\\u0445\\u0430\\u043d\\u043e\\u043c\\u0443 \\u043e\\u0431\\u043b\\u0438\\u0447\\u0447\\u0456!<\\/p>\",\"visible\":\"1\",\"photo\":\"63c573f894b02.png\"},\"count\":1}],\"total_price\":1000}', 1000),
(5, 9, 'Mark', 'Alien', '0931234343', 'mark_alien@gmail.com', '{\"products\":[{\"product\":{\"id\":\"22\",\"name\":\"\\u0411\\u0443\\u043a\\u0435\\u0442 \\\"7 \\u0431\\u0456\\u043b\\u0438\\u0445 \\u0456 \\u0440\\u043e\\u0436\\u0435\\u0432\\u0438\\u0445 \\u0442\\u044e\\u043b\\u044c\\u043f\\u0430\\u043d\\u0456\\u0432\\\"\",\"category_id\":\"7\",\"price\":\"400\",\"count\":\"20\",\"description\":\"<p>\\u041d\\u0456\\u0436\\u043d\\u0438\\u0439 \\u0431\\u0443\\u043a\\u0435\\u0442 \\u0432\\u0435\\u0441\\u043d\\u044f\\u043d\\u0438\\u0445 \\u0442\\u044e\\u043b\\u044c\\u043f\\u0430\\u043d\\u0456\\u0432.<\\/p>\",\"visible\":\"1\",\"photo\":\"63c5761b3f345.png\"},\"count\":1},{\"product\":{\"id\":\"8\",\"name\":\"\\u0427\\u0435\\u0440\\u0432\\u043e\\u043d\\u0430 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434\\u0430 (\\u043f\\u043e\\u0448\\u0442\\u0443\\u0447\\u043d\\u043e)\",\"category_id\":\"1\",\"price\":\"25\",\"count\":\"100\",\"description\":\"<p><strong>\\u0427\\u0435\\u0440\\u0432\\u043e\\u043d\\u0430 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434\\u0430<\\/strong> - \\u0441\\u0438\\u043c\\u0432\\u043e\\u043b \\u043b\\u044e\\u0431\\u043e\\u0432\\u0456, \\u043f\\u0440\\u0438\\u0441\\u0442\\u0440\\u0430\\u0441\\u0442\\u0456 \\u0442\\u0430 \\u0432\\u0456\\u0434\\u0434\\u0430\\u043d\\u043e\\u0441\\u0442\\u0456. \\u0421\\u043a\\u043b\\u0430\\u0434\\u0456\\u0442\\u044c \\u0432\\u043b\\u0430\\u0441\\u043d\\u0438\\u0439 \\u0431\\u0443\\u043a\\u0435\\u0442 \\u0437 \\u0431\\u0443\\u0434\\u044c-\\u044f\\u043a\\u043e\\u0457 \\u043a\\u0456\\u043b\\u044c\\u043a\\u043e\\u0441\\u0442\\u0456 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434!<\\/p>\",\"visible\":\"1\",\"photo\":\"63c56dcb49439.png\"},\"count\":11}],\"total_price\":675}', 675),
(6, 12, 'Kyle', 'Tyler', '0937442486', 'kyleeee@gmail.com', '{\"products\":[{\"product\":{\"id\":\"8\",\"name\":\"\\u0427\\u0435\\u0440\\u0432\\u043e\\u043d\\u0430 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434\\u0430 (\\u043f\\u043e\\u0448\\u0442\\u0443\\u0447\\u043d\\u043e)\",\"category_id\":\"1\",\"price\":\"25\",\"count\":\"89\",\"description\":\"<p><strong>\\u0427\\u0435\\u0440\\u0432\\u043e\\u043d\\u0430 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434\\u0430<\\/strong> - \\u0441\\u0438\\u043c\\u0432\\u043e\\u043b \\u043b\\u044e\\u0431\\u043e\\u0432\\u0456, \\u043f\\u0440\\u0438\\u0441\\u0442\\u0440\\u0430\\u0441\\u0442\\u0456 \\u0442\\u0430 \\u0432\\u0456\\u0434\\u0434\\u0430\\u043d\\u043e\\u0441\\u0442\\u0456. \\u0421\\u043a\\u043b\\u0430\\u0434\\u0456\\u0442\\u044c \\u0432\\u043b\\u0430\\u0441\\u043d\\u0438\\u0439 \\u0431\\u0443\\u043a\\u0435\\u0442 \\u0437 \\u0431\\u0443\\u0434\\u044c-\\u044f\\u043a\\u043e\\u0457 \\u043a\\u0456\\u043b\\u044c\\u043a\\u043e\\u0441\\u0442\\u0456 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434!<\\/p>\",\"visible\":\"1\",\"photo\":\"63c56dcb49439.png\"},\"count\":15},{\"product\":{\"id\":\"15\",\"name\":\"\\u0411\\u0443\\u043a\\u0435\\u0442  \\\"15 \\u0447\\u0435\\u0440\\u0432\\u043e\\u043d\\u0438\\u0445 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434\\\"\",\"category_id\":\"1\",\"price\":\"700\",\"count\":\"15\",\"description\":\"<p>\\u0428\\u0438\\u043a\\u0430\\u0440\\u043d\\u0456&nbsp;\\u0447\\u0435\\u0440\\u0432\\u043e\\u043d\\u0456&nbsp;\\u0442\\u0440\\u043e\\u044f\\u043d\\u0434\\u0438&nbsp;-&nbsp;\\u043d\\u0430\\u0439\\u043a\\u0440\\u0430\\u0449\\u0438\\u0439&nbsp;\\u043f\\u043e\\u0434\\u0430\\u0440\\u0443\\u043d\\u043e\\u043a.<\\/p>\",\"visible\":\"1\",\"photo\":\"63c57318c3c25.png\"},\"count\":1}],\"total_price\":1075}', 1075),
(7, 2, 'Human', 'Humanoid', '0672853958', 'helloimhuman@gmail.com', '{\"products\":[{\"product\":{\"id\":\"21\",\"name\":\"\\u0411\\u0443\\u043a\\u0435\\u0442 \\\"\\u041f\\u043e\\u0437\\u0438\\u0442\\u0438\\u0432\\\"\",\"category_id\":\"3\",\"price\":\"899\",\"count\":\"10\",\"description\":\"<p>6 \\u0433\\u0456\\u043b\\u043e\\u043a \\u0431\\u0456\\u043b\\u043e\\u0457 \\u043a\\u0443\\u0449\\u043e\\u0432\\u043e\\u0457 \\u0445\\u0440\\u0438\\u0437\\u0430\\u043d\\u0442\\u0435\\u043c\\u0438, 7 <strong>\\u0440\\u0456\\u0437\\u043d\\u043e\\u043a\\u043e\\u043b\\u044c\\u043e\\u0440\\u043e\\u0432\\u0438\\u0445 \\u0433\\u0435\\u0440\\u0431\\u0435\\u0440<\\/strong>, \\u043e\\u0444\\u043e\\u0440\\u043c\\u043b\\u0435\\u043d\\u043d\\u044f \\u043c\\u043e\\u0436\\u0435 \\u0432\\u0456\\u0434\\u0440\\u0456\\u0437\\u043d\\u044f\\u0442\\u0438\\u0441\\u044f.<\\/p>\",\"visible\":\"1\",\"photo\":\"63c575a5e2b00.png\"},\"count\":1}],\"total_price\":899}', 899),
(8, 13, 'Alien', 'Alien', '0935944756', 'dfa@gmail.com', '{\"products\":[{\"product\":{\"id\":\"22\",\"name\":\"\\u0411\\u0443\\u043a\\u0435\\u0442 \\\"7 \\u0431\\u0456\\u043b\\u0438\\u0445 \\u0456 \\u0440\\u043e\\u0436\\u0435\\u0432\\u0438\\u0445 \\u0442\\u044e\\u043b\\u044c\\u043f\\u0430\\u043d\\u0456\\u0432\\\"\",\"category_id\":\"7\",\"price\":\"400\",\"count\":\"19\",\"description\":\"<p>\\u041d\\u0456\\u0436\\u043d\\u0438\\u0439 \\u0431\\u0443\\u043a\\u0435\\u0442 \\u0432\\u0435\\u0441\\u043d\\u044f\\u043d\\u0438\\u0445 \\u0442\\u044e\\u043b\\u044c\\u043f\\u0430\\u043d\\u0456\\u0432.<\\/p>\",\"visible\":\"1\",\"photo\":\"63c5761b3f345.png\"},\"count\":1},{\"product\":{\"id\":\"8\",\"name\":\"\\u0427\\u0435\\u0440\\u0432\\u043e\\u043d\\u0430 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434\\u0430 (\\u043f\\u043e\\u0448\\u0442\\u0443\\u0447\\u043d\\u043e)\",\"category_id\":\"1\",\"price\":\"25\",\"count\":\"74\",\"description\":\"<p><strong>\\u0427\\u0435\\u0440\\u0432\\u043e\\u043d\\u0430 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434\\u0430<\\/strong> - \\u0441\\u0438\\u043c\\u0432\\u043e\\u043b \\u043b\\u044e\\u0431\\u043e\\u0432\\u0456, \\u043f\\u0440\\u0438\\u0441\\u0442\\u0440\\u0430\\u0441\\u0442\\u0456 \\u0442\\u0430 \\u0432\\u0456\\u0434\\u0434\\u0430\\u043d\\u043e\\u0441\\u0442\\u0456. \\u0421\\u043a\\u043b\\u0430\\u0434\\u0456\\u0442\\u044c \\u0432\\u043b\\u0430\\u0441\\u043d\\u0438\\u0439 \\u0431\\u0443\\u043a\\u0435\\u0442 \\u0437 \\u0431\\u0443\\u0434\\u044c-\\u044f\\u043a\\u043e\\u0457 \\u043a\\u0456\\u043b\\u044c\\u043a\\u043e\\u0441\\u0442\\u0456 \\u0442\\u0440\\u043e\\u044f\\u043d\\u0434!<\\/p>\",\"visible\":\"1\",\"photo\":\"63c56dcb49439.png\"},\"count\":5}],\"total_price\":525}', 525),
(9, 14, 'Test', 'New', '0673984627', 'test@gmail.com', '{\"products\":[{\"product\":{\"id\":\"25\",\"name\":\"\\u0411\\u0443\\u043a\\u0435\\u0442 \\\"9 \\u043d\\u0456\\u0436\\u043d\\u0438\\u0445 \\u043e\\u0440\\u0445\\u0456\\u0434\\u0435\\u0439\\\"\",\"category_id\":\"9\",\"price\":\"900\",\"count\":\"8\",\"description\":\"<p>9 \\u043a\\u0432\\u0456\\u0442\\u043e\\u043a \\u0431\\u0456\\u043b\\u0438\\u0445 \\u0442\\u0430 \\u0440\\u043e\\u0436\\u0435\\u0432\\u0438\\u0445 \\u043e\\u0440\\u0445\\u0456\\u0434\\u0435\\u0439, \\u0437\\u0435\\u043b\\u0435\\u043d\\u044c, \\u043e\\u0444\\u043e\\u0440\\u043c\\u043b\\u0435\\u043d\\u043d\\u044f.<\\/p>\",\"visible\":\"1\",\"photo\":\"63c57751b6fe3.png\"},\"count\":1},{\"product\":{\"id\":\"38\",\"name\":\"\\u0411\\u0443\\u043a\\u0435\\u0442 \\\"\\u041f\\u0435\\u0440\\u043b\\u0438\\u043d\\u0430\\\"\",\"category_id\":\"17\",\"price\":\"3990\",\"count\":\"10\",\"description\":\"<p>\\u041f\\u043e\\u0432\\u0456\\u0442\\u0440\\u044f\\u043d\\u0438\\u0439, \\u0433\\u0430\\u0440\\u043d\\u0438\\u0439 \\u0442\\u0430 \\u043d\\u0456\\u0436\\u043d\\u0438\\u0439! \\u0412\\u0456\\u0434\\u043c\\u0456\\u043d\\u043d\\u0438\\u0439 \\u0432\\u0430\\u0440\\u0456\\u0430\\u043d\\u0442, \\u0449\\u043e\\u0431 \\u043f\\u043e\\u0440\\u0430\\u0434\\u0443\\u0432\\u0430\\u0442\\u0438 \\u0431\\u043b\\u0438\\u0437\\u044c\\u043a\\u0438\\u0445 \\u0431\\u0435\\u0437 \\u0437\\u0430\\u0439\\u0432\\u0438\\u0445 \\u0441\\u043b\\u0456\\u0432 \\u0442\\u0430 \\u043f\\u0440\\u0438\\u0432\\u043e\\u0434\\u0443!<\\/p>\",\"visible\":\"1\",\"photo\":\"63c57e24bc173.png\"},\"count\":3}],\"total_price\":12870}', 12870);

-- --------------------------------------------------------

--
-- Структура таблиці `product`
--

CREATE TABLE `product` (
  `id` int NOT NULL COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT 'Назва товару',
  `category_id` int DEFAULT NULL COMMENT 'Категорія товару',
  `price` float NOT NULL COMMENT 'Ціна',
  `count` int NOT NULL COMMENT 'Кількість',
  `description` text COMMENT 'Короткий опис',
  `visible` int NOT NULL DEFAULT '1' COMMENT 'Видимість товару',
  `photo` varchar(255) NOT NULL COMMENT 'Назва фотографії	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп даних таблиці `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `price`, `count`, `description`, `visible`, `photo`) VALUES
(8, 'Червона троянда (поштучно)', 1, 25, 69, '<p><strong>Червона троянда</strong> - символ любові, пристрасті та відданості. Складіть власний букет з будь-якої кількості троянд!</p>', 1, '63c56dcb49439.png'),
(9, 'Біла троянда (поштучно)', 1, 30, 100, '<p>Подаруйте букет з будь-якої кількості гарних та свіжих <strong>білосніжних троянд.</strong></p>', 1, '63c56e96c24c6.png'),
(10, 'Жовта троянда (поштучно)', 1, 24, 100, '<p><strong>Жовті троянди</strong> в букеті означають прояв турботи. Жовтий колір означає свободу і енергію. Жовті троянди часто дарують&nbsp;на свято.</p>', 1, '63c56f73a4c37.png'),
(11, 'Рожева троянда (поштучно)', 1, 25, 100, '<p><strong>Рожеві троянди</strong> є символом елегантності та вишуканості.</p>', 1, '63c5700d175be.png'),
(12, 'Букет \"11 кремових троянд\"', 1, 550, 20, '<p>Дуже лаконічний та заворожуючий букет ніжних кремових троянд.</p>', 1, '63c570b5a6035.png'),
(13, 'Букет троянд \"Карнавал\"', 1, 1000, 15, '<p>Незвичайна комбінація класики і модерна, цей букет з різноколірних троянд стане хорошою альтернативою традиційним букетам з червоних або рожевих троянд.</p>', 1, '63c5720f9f568.png'),
(14, 'Букет \"Святковий\"', 1, 999, 12, '<p>Шикарне поєднання ніжних кремових троянд, кущових рожевих троянд і оригінальної упаковки подарує одержувачу позитивні емоції і надовго закарбується в пам\'яті!</p>', 1, '63c5728881ba1.png'),
(15, 'Букет  \"15 червоних троянд\"', 1, 700, 14, '<p>Шикарні&nbsp;червоні&nbsp;троянди&nbsp;-&nbsp;найкращий&nbsp;подарунок.</p>', 1, '63c57318c3c25.png'),
(16, 'Букет \"25 різнокольорових хризантем\"', 2, 1500, 12, '<p>Дивовижне поєднання їх простоти і яскравості залишить в душі Вашого одержувача найтепліші почуття.</p>', 1, '63c573802bad6.png'),
(17, 'Букет \"Яскраві сонечка\"', 2, 1000, 11, '<p>Ясні сонечка хризантем порадують дорогого одержувача і подарують посмішку на коханому обличчі!</p>', 1, '63c573f894b02.png'),
(18, 'Букет \"Свіже рішення\"', 2, 2000, 7, '<p>35 гілок рожевої хризантеми кущової, оформлення може відрізнятися.</p>', 1, '63c5747b1c05d.png'),
(19, 'Квіти в коробці \"Зелена галявина\"', 2, 1200, 6, '<p>Незвичайні міні-хризантеми Сантіні – дивовижний та незвичайний варіант подарунка. Яскраві кулясті квіти привертають увагу та захоплюють!</p>', 1, '63c574dac55b1.png'),
(20, 'Букет \"Промені сонця\"', 3, 800, 20, '<p>Яскраві сонечки гербери зігріють у будь-яку погоду і неодмінно подарують позитивні емоції на весь день!</p>', 1, '63c57539edbc2.png'),
(21, 'Букет \"Позитив\"', 3, 899, 9, '<p>6 гілок білої кущової хризантеми, 7 <strong>різнокольорових гербер</strong>, оформлення може відрізнятися.</p>', 1, '63c575a5e2b00.png'),
(22, 'Букет \"7 білих і рожевих тюльпанів\"', 7, 400, 18, '<p>Ніжний букет весняних тюльпанів.</p>', 1, '63c5761b3f345.png'),
(23, 'Букет \"9 рожевих тюльпанів\"', 7, 450, 15, '<p>Ніжний&nbsp;букет&nbsp;весняних&nbsp;тюльпанів&nbsp;підніме&nbsp;настрій&nbsp;Вашому&nbsp;одержувачу!</p>', 1, '63c5765f9e47e.png'),
(24, 'Букет \"Мрія\"', 8, 989, 16, '<p>Подаруйте яскраву мрію!</p>', 1, '63c576d0b37d5.png'),
(25, 'Букет \"9 ніжних орхідей\"', 9, 900, 7, '<p>9 квіток білих та рожевих орхідей, зелень, оформлення.</p>', 1, '63c57751b6fe3.png'),
(26, 'Букет \"Весна\"', 10, 1299, 15, '<p>11 гілок рожевої та кремової кущової троянди.</p>', 1, '63c577fdcf9d9.png'),
(27, 'Букет \"Краса\"', 10, 799, 14, '<p>Ніжний і витончений шик в букеті з кремових троянд - хороший вибір для привітання.</p>', 1, '63c578720d40d.png'),
(28, 'Букет \"Пастель\"', 11, 1700, 15, '<p>Ніжний і стильний букет в пастельних тонах стане чудовим подарунком!</p>', 1, '63c57900bbf52.png'),
(29, 'Букет \"7 ніжних альстромерій\"', 15, 499, 20, '<p>Ніжний мікс квітів безсумнівно зачарує Вашого одержувача!</p>', 1, '63c5794bd8f8e.png'),
(30, 'Букет \"9 рожевих альстромерій\"', 15, 699, 15, '<p>Ніжний букет альстромерій безсумнівно зачарує Вашого одержувача!</p>', 1, '63c579898d79e.png'),
(31, 'Букет \"Квітучий сад\"', 16, 1800, 10, '<p>Чудовий мікс яскравих і ароматних квітів передасть найпрекрасніші і теплі побажання Вашому одержувачу!</p>', 1, '63c579f9e9c65.png'),
(32, 'Букет \"Для прекрасної людини\"', 16, 2900, 14, '<p>Дуже ніжний і чаруючий букет - ідеальне привітання для витонченої натури!</p>', 1, '63c57a6d04e39.png'),
(33, 'Букет \"Квіткове Selfie!\"', 16, 1850, 14, '<p>Стильний літній букет зарядить Вашого одержувача позитивом і приємними емоціями!</p>', 1, '63c57ab2170eb.png'),
(34, 'Букет \"Гармонія почуттів!\"', 16, 2599, 15, '<p>Квіткова&nbsp;розкіш&nbsp;подарує чудовий&nbsp;настрій&nbsp;в&nbsp;чарівний&nbsp;весняний&nbsp;день!</p>', 1, '63c57aef0bee7.png'),
(35, 'Букет \"51 кущова троянда\"', 17, 4100, 6, '<p>51 гілка&nbsp;кремової і рожевої кущової троянди&nbsp;(відтінок і сорт троянди може відрізнятися від представленого на фото), зелень, оформлення&nbsp;може відрізнятися.</p>', 1, '63c57b60e26ff.png'),
(36, 'Букет \"101 рожевий тюльпан\"', 17, 3999, 7, '<p>Оберемок ніжних весняних тюльпанів порадує Вашого одержувача і подарує весняний настрій після довгої і сніжної зими!</p>', 1, '63c57ba884ab1.png'),
(37, 'Букет \"101 різнокольорова троянда\"', 17, 4999, 10, '<p>Різнобарвний мікс із троянд - чудовий яскравий феєрверк емоцій.</p>', 1, '63c57c40ccc74.png'),
(38, 'Букет \"Перлина\"', 17, 3990, 7, '<p>Повітряний, гарний та ніжний! Відмінний варіант, щоб порадувати близьких без зайвих слів та приводу!</p>', 1, '63c57e24bc173.png');

-- --------------------------------------------------------

--
-- Структура таблиці `reviews`
--

CREATE TABLE `reviews` (
  `id` int NOT NULL COMMENT 'ID',
  `user_id` int NOT NULL COMMENT 'ID користувача',
  `user_rating` int NOT NULL COMMENT 'Оцінка користувача',
  `user_review` text NOT NULL COMMENT 'Відгук',
  `date_and_time` varchar(255) NOT NULL COMMENT 'Дата і час',
  `product_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп даних таблиці `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `user_rating`, `user_review`, `date_and_time`, `product_id`) VALUES
(33, 1, 4, 'Good flowers.', '2023.01.15', 4),
(34, 1, 5, 'I like the fact that they are blue, very charming', '2023.01.15', 4),
(35, 1, 3, 'wow', '2023.01.15', 4),
(36, 1, 3, 'asdfasdfasdfasdf', '2023.01.15', 3),
(37, 1, 5, 'asdasd', '2023.01.15', 4),
(38, 1, 5, 'asdfasdfasdf', '2023.01.15', 7),
(39, 1, 5, 'sfasfsfsf', '2023.01.15', 3),
(40, 1, 5, 'boom !', '2023.01.15', 3),
(41, 1, 5, 'dsfasdfASDFASDFASDF', '2023.01.15', 3),
(42, 2, 5, 'Гарні, свіжі троянди!', '2023.01.16', 8),
(43, 2, 4, 'Гарний букет.', '2023.01.16', 27),
(44, 10, 5, 'Квіти мають дуже витончений аромат!', '2023.01.16', 9),
(45, 2, 3, 'Один листочок на квітці був сухий :(', '2023.01.16', 9),
(46, 13, 4, 'фвіафівафівафіваіфв', '2023.01.17', 8);

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL COMMENT 'ID',
  `login` varchar(255) NOT NULL COMMENT 'Логін',
  `password` varchar(255) NOT NULL COMMENT 'Пароль',
  `lastname` varchar(255) NOT NULL COMMENT 'Прізвище',
  `firstname` varchar(255) NOT NULL COMMENT 'Ім''я',
  `email` varchar(255) NOT NULL COMMENT 'Email',
  `phoneNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Номер телефону',
  `access_level` int NOT NULL DEFAULT '1' COMMENT 'Рівень доступу'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `lastname`, `firstname`, `email`, `phoneNumber`, `access_level`) VALUES
(1, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 'Адмін', 'Адмін', 'admin@gmail.com', '0938266482', 10),
(2, 'Human', '827ccb0eea8a706c4c34a16891f84e7b', 'Humanoid', 'Human', 'helloimhuman@gmail.com', '0672853958', 1),
(4, 'HashTest', 'e10adc3949ba59abbe56e057f20f883e', 'test', 'test', 'erehgwe@gmail.com', '0934567483', 1),
(5, 'NewGenAdmin', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin', 'Admin', 'newgenadmin@gmail.com', '0935639367', 10),
(6, 'Miko44', '27caa6c8fc6449b6f98cee1e52f89090', 'Kimo', 'Yamaguchi', '0672939104', 'kimiko_yamaguchi@gmail.com', 1),
(7, 'SomeUser', '827ccb0eea8a706c4c34a16891f84e7b', 'User', 'User', 'some_user@gmail.com', '0932345435', 1),
(8, 'Miku', 'f15d5ce2b557c1ea187b2c616f0762d9', 'Hatsune', 'Miku', 'hatsune_miku@gmail.com', '0673947274', 1),
(9, 'Alien44', 'b59c67bf196a4758191e42f76670ceba', 'Alien', 'Mark', 'mark_alien@gmail.com', '0931234343', 1),
(10, 'FlowerLover', '25d55ad283aa400af464c76d713c07ad', 'Flower', 'Lover', 'asfdasfasf@gmail.com', '0673451232', 1),
(11, 'Mikuuu', '1bbd886460827015e5d605ed44252251', 'Yamaguchi', 'Kimiko', 'kimiko_yamaguchi@gmail.com', '0935345432', 1),
(12, 'Kyle', '93279e3308bdbbeed946fc965017f67a', 'Tyler', 'Kyle', 'kyleeee@gmail.com', '0937442486', 1),
(13, 'User', '827ccb0eea8a706c4c34a16891f84e7b', 'Alien', 'Alien', 'dfa@gmail.com', '0935944756', 1),
(14, 'new_test_user', '827ccb0eea8a706c4c34a16891f84e7b', 'New', 'Test', 'test@gmail.com', '0673984627', 1);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_ibfk_1` (`user_id`);

--
-- Індекси таблиці `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Індекси таблиці `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_ibfk_1` (`user_id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `category`
--
ALTER TABLE `category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID категорії', AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблиці `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблиці `product`
--
ALTER TABLE `product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT для таблиці `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=15;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
